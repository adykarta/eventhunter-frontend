import React, {Component} from 'react';
import "../home/header.css";

class Header extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <div className="HeaderWrapper">
                <img className="imgHeader" src="/img/event.jpg"/>
                <div className="HeaderOverlay">
                    <h1>EVENTS MADE EASY</h1>
                    <h3>Bookings, Registrations, Sell Tickets Online</h3>
                    <button type="button" class="btn btn-outline-light btn-lg">Book Tickets</button>
                    <button type="button" class="btn btn-outline-light btn-lg">Sell Tickets</button>
                </div>
            </div>
        );
    }
}
export default Header;