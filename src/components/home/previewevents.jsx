import React, {Component} from "react";
import "../home/previewevents.css";
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

class Previewevents extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <div className="previewWrapper">
                <div className="seeButton">
                    <button type="button" class="btn btn-info btn-lg">See All  --></button>
                </div>
                <OwlCarousel
                    className="owl-theme"
                    loop={true}
                    margin={100}
                    items={3}
                    center={true}
                    nav={false}
                    responsiveClass={true}
                    autoWidth={true}
                    autoplay={true}
                >
                    <div class="item">
                        <div class="card">
                            <img class="card-img-top" src="" alt="Card image cap" />
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                <a href="#" class="btn btn-primary">Go somewhere</a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card">
                            <img class="card-img-top" src="" alt="Card image cap" />
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                <a href="#" class="btn btn-primary">Go somewhere</a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card">
                            <img class="card-img-top" src="" alt="Card image cap" />
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                <a href="#" class="btn btn-primary">Go somewhere</a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card">
                            <img class="card-img-top" src="" alt="Card image cap" />
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                <a href="#" class="btn btn-primary">Go somewhere</a>
                            </div>
                        </div>
                    </div>
                   
                   
                    
                </OwlCarousel>

            </div>
        )
    }
}
export default Previewevents;