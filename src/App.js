import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Header from './components/home/header';
import Previewevents from './components/home/previewevents';
import Aboutus from './components/home/aboutus';
import Navbar from './components/navbar';

const Home = () => (
    <div>
        <Header />
        <Navbar />
        <Previewevents />
        <Aboutus />
    </div>

);



class App extends Component {
    render() {
        return (
            <Router>
                <div className="App">
                    <Switch>
                        <Route exact path="/" component={Home} />
                    </Switch>
        
                </div>
            </Router>
        );
    }
}

export default App;
